import 'package:flutter/material.dart';

class ImageCardWidget extends StatelessWidget{
  
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    // return const Image(image: AssetImage('images/pemandangan.jpg'));
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        ImageCard(imagePath:'images/pemuda.jpg'),
        ImageCard(imagePath:'images/pemandangan.jpg'),
        ImageCard(imagePath:'images/pdip.png'),
      ],
    );
  }

}

class ImageCard extends StatelessWidget {
  final String imagePath;

  const ImageCard({super.key, required this.imagePath});
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            const CircleAvatar(
              backgroundImage: AssetImage('images/pdip.png'),
              radius: 30,
              ),
                
              Text("Lathief"),
              Text("Kevin")
        ],),
        Image.asset(imagePath),
        Text("Footer")        
      ],
    );
  }
}